<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>账户管理 - 携程礼品卡</title>
    <style>
        body {
            width: 80%;
            margin: 0 auto;
        }

        form {
            text-align: center;
        }

        label {
            background-color: #7f76dc;
            display: inline-block;
            color: white;
            line-height: 30px;
            padding: 5px 10px;
        }

        input {
            width: 80%;
            margin: 1pc auto;
            height: 30px;
            padding: 5px 10px;
            outline: none;
        }

        #cookie {
            display: block;
            width: 85%;
            margin: 0 auto;
            height: 200px;
            outline: none;
            font-family: Consolas, serif;
            padding: 8px;
            word-break: break-all;
        }
    </style>
</head>
<body>
<h1 style="text-align: center">账户管理 - 携程礼品卡</h1>
<form action="index.php?cookie" method="post">
    <input type="hidden" name="new"
           value=<?php echo (empty($_GET['a']) && empty($_GET['p']) && empty($_GET['y'])) ? 'yes' : 'no' ?>>

    <div>
        <label>登录账号</label>
        <input type="text" name="account" autocomplete="off" required placeholder="账号"
               value=<?php echo empty($_GET['a']) ? '' : $_GET['a'] . ' readonly'; ?>>
    </div>

    <div>
        <label>登录密码</label>
        <input type="text" name="password" autocomplete="off" required placeholder="登录密码"
               value=<?php echo empty($_GET['p']) ? '' : $_GET['p'] . ' readonly'; ?>>
    </div>

    <div>
        <label>支付密码</label>
        <input type="text" name="passpay" autocomplete="off" required placeholder="支付密码"
               value=<?php echo empty($_GET['y']) ? '' : $_GET['y'] . ' readonly'; ?>>
    </div>

    <div>
        <label>cookie</label>
        <textarea name="cookies" placeholder="在此处粘贴cookie" required
                  id="cookie" <?php echo isset($_GET['s']) ? 'readonly' : ''; ?>><?php echo isset($_GET['s']) ? 'SET-PAY-PASSWORD' : ''; ?></textarea>
    </div>
    <div>
        <input type="submit" value="提交" style="width: 20%">
    </div>
</form>

<p style="text-align: center"><a href='index.php' style='color: blue'>返回礼品卡管理</a></p>

<hr>
<h2>如何从携程官网获取cookie？</h2>
<p>电话（同微信）：15738513611</p>
</body>
</html>
