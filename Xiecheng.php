<?php

namespace lifanko;

use PDO;
use PDOException;

class Xiecheng
{
    public function post($url, $header, $param)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }

    public function get_money($cookie)
    {
        $url_info = 'https://zc.ctrip.com/getGiftCardValidityInfoList';

        $header = array('content-type: application/json', 'cookie: ' . $cookie, 'x_token_value: RuaFSN58QDXCA%2BomxtBwPN2CP6V1SpXFzP2arPxZuBXiFSQ2DOmy3R3b83Y2iyyj');

        $param = '{"ticketCategoryId":0,"pageIndex":1,"pageSize":8}';

        return $this->post($url_info, $header, $param);
    }

    public function get_info($cookie)
    {
        $url_info = 'https://m.ctrip.com/restapi/soa2/11838/getUserInfo';

        $header = array('content-type: application/json', 'cookie: ' . $cookie, 'x_token_value: RuaFSN58QDXCA%2BomxtBwPN2CP6V1SpXFzP2arPxZuBXiFSQ2DOmy3R3b83Y2iyyj');

        $param = '{"parameterList":[{"key":"BizType","value":"BASE"}],"queryConditionList":[{"key":"Self","value":"1"}]}';

        return $this->post($url_info, $header, $param);
    }

    public function get_order($cookie)
    {
        $url_info = 'https://zc.ctrip.com/getGCTransDetail';

        $header = array('content-type: application/json', 'cookie: ' . $cookie, 'x_token_value: RuaFSN58QDXCA%2BomxtBwPN2CP6V1SpXFzP2arPxZuBXiFSQ2DOmy3R3b83Y2iyyj');

        $param = '{"ticketCategoryId":0,"paymentType":0,"pageIndex":1,"pageSize":20}';

        return $this->post($url_info, $header, $param);
    }

    public function get_amount($cookie)
    {
        $url_info = 'https://zc.ctrip.com/getUaAmount';

        $header = array('content-type: application/json', 'cookie: ' . $cookie, 'x_token_value: RuaFSN58QDXCA%2BomxtBwPN2CP6V1SpXFzP2arPxZuBXiFSQ2DOmy3R3b83Y2iyyj');

        return $this->post($url_info, $header, '');
    }

    public function pdo($host, $dbname, $user, $pass = '')
    {
        try {
            $pdo = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
        } catch (PDOException $e) {
            die("<div style='color: red;text-align: center;margin-top: 10%'><h1>Unable to connect to database</h1><h3>Please contact lifankohome@163.com</h3></div>");
        }
        $pdo->query("set names utf8");

        return $pdo;
    }
}
