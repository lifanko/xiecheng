<?php
if (empty($_GET['account'])) {
    die('<h3 style="margin-top: 20%;text-align: center;color: #F40;">参数不足，无法查询消费记录</h3>');
}

include_once 'Xiecheng.php';

use lifanko\Xiecheng;

$xiecheng = new Xiecheng();

$db_config = json_decode(file_get_contents('db.json'));
$pdo = $xiecheng->pdo($db_config->host, $db_config->dbname, $db_config->username, $db_config->password);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>订单记录 - 携程礼品卡</title>
    <style>
        body {
            min-width: 960px;
            width: 70%;
            margin: 0 auto;
        }

        .info {
            font-size: 20px;
            margin: 10px;
        }

        .info span {
            width: 33.3%;
            display: inline-block;
            text-align: center;
        }

        table {
            text-align: center;
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            word-break: break-all;
            word-wrap: break-word;
            font-size: 14px;
            font-family: "Microsoft JhengHei UI", serif;
        }

        table th {
            font-weight: bold;
            background: #efefef;
            padding: 10px;
            border: 1px solid #dfdfdf;
        }

        table td {
            border: 1px solid #dfdfdf;
            padding: 12px;
        }
    </style>
</head>
<body>
<h1 style="text-align: center">订单记录 - 携程礼品卡</h1>

<?php
$tb = 'list';

$account = $_GET['account'];

$sql = "SELECT name,money,cookie FROM $tb WHERE account=:account";
$stmt = $pdo->prepare($sql);
$stmt->execute(['account' => $account]);
$res = $stmt->fetch(PDO::FETCH_ASSOC);

echo "<div class='info'><span>账号：$account </span><span>账户名：{$res['name']} </span><span>余额：￥{$res['money']}</span></div>";

$orders = $xiecheng->get_order($res['cookie']);
$orders = json_decode($orders, true);

if ($orders['code'] != 0) {
    die('<h3 style="margin-top: 20%;text-align: center;color: #F40;">cookie已失效，请更新</h3>');
}
?>
<table>
    <tr>
        <th>交易时间</th>
        <th>卡类型</th>
        <th>交易类型</th>
        <th>收支</th>
        <th>流水号</th>
        <th>订单号</th>
        <th>备注</th>
    </tr>
    <?php
    foreach ($orders['data']['ticketPaymentDetail'] as $val) {
        echo "<tr>";

        echo "<td>{$val['transTime']}</td>";
        echo "<td>{$val['ticketCategoryName']}</td>";
        echo "<td>{$val['transDetailTypeName']}</td>";
        echo "<td>{$val['transAmount']}</td>";
        echo "<td>{$val['transSequence']}</td>";
        echo "<td>{$val['orderId']}</td>";
        echo "<td>{$val['remark']}</td>";

        echo "</tr>";
    }
    ?>
</table>
<p style="text-align: center"><a href='index.php' style='color: blue'>返回礼品卡管理</a></p>
</body>
</html>
